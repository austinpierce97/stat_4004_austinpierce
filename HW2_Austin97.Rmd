---
title: "HW2_Austin97"
author: "Austin Pierce"
date: "January 25, 2019"
output: pdf_document
---
\begin{itemize}
\item Question 4:
\end{itemize}

```{R}
rep(1:5, c(1,2,3,4,5))

cbind(c(0,0,7), c(2,5,0), c(3,0,0))
```

\begin{itemize}
\item Question 5
\end{itemize}

\begin{itemize}
\item N = 100: Partition the integration range into segments
\end{itemize}

```{R}
n.seg <- 100

segments <- seq(from = 1, to = 2, length.out = n.seg +1)

# Calculate evaluation point

eval.points <- (segments[1:n.seg] + segments[2:n.seg + 1])/2

#Calculate width

del <- (2/n.seg)

# Evaluation funtion at evaluation points

f.t <- (1/sqrt(2*pi))*(exp(-(eval.points^2)/2))

# Sum area of the rectangle

sum(f.t*del)
```

\begin{itemize}
\item N = 1000: Partition the integration range into segments
\end{itemize}

```{R}
n.seg <- 1000

segments <- seq(from = 1, to = 2, length.out = n.seg +1)

# Calculate evaluation point

eval.points <- (segments[1:n.seg] + segments[2:n.seg + 1])/2

#Calculate width

del <- (2/n.seg)

#Evaluation funtion at evaluation points

f.t <- (1/sqrt(2*pi))*(exp(-(eval.points^2)/2))

# Sum area of the rectangle

sum(f.t*del)
```

\begin{itemize}
\item N = 10000: Partition the integration range into segments
\end{itemize}

```{R}
n.seg <- 10000

segments <- seq(from = 1, to = 2, length.out = n.seg +1)

# Calculate evaluation point

eval.points <- (segments[1:n.seg] + segments[2:n.seg + 1])/2

# Calculate width

del <- (2/n.seg)

# Evaluation funtion at evaluation points

f.t <- (1/sqrt(2*pi))*(exp(-(eval.points^2)/2))

# Sum area of the rectangle

sum(f.t*del)

#This sum appears to be converging to .2718
```

\begin{itemize}
\item Question 6:
\end{itemize}

```{R}
#Assign vector x

x <- c(9,-3,2,6)

sd(x)

#By hand the standard deviation is: 5.1962

#These are the same standard deviations.
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.
